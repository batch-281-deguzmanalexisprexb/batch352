@extends('layouts.app')

@section('content')

	<div class="card">
		<div class="card-body">
			<h2 class="card-title">{{$post->title}}</h2>
			<p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class="card-subtitle text-muted mb-3">Created at: {{$post->created_at}}</p>
			<p class="card-text">{{$post->content}}</p>

			@if(Auth::id() != $post->user_id)
				<form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
					@method('PUT')
					@csrf
					@if($post->likes->contains("user_id", Auth::id()))
						<button type="submit" class="btn btn-danger">Unlike</button>
					@else
						<button type="submit" class="btn btn-success">Like</button>
					@endif
				</form>
			@endif

			<p>Likes: {{count($postLike)}} | Comments: {{count($postComment)}}</p>

			@if(Auth::id())
			<button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">Comment</button>
			@endif

			<div class="mt-3">
				<a href="/posts" class="card-link">View All Posts</a>
			</div>
		</div>

		<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<form method="POST" action="/posts/{{$post->id}}/comment"> 
						@method('PUT')
						@csrf
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Leave a comment</h5>
							<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
						</div>
						<div class="modal-body">
							<label for="content">Comment:</label>
							<textarea class="form-control" id="content" name="content" rows="3"></textarea>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Comment</button>
						</div>
					</form> 
				</div>
			</div>
		</div>		
	</div>

	<p class="mt-5">Comments: </p>

	@foreach($postComment as $postComment)
        <div class="card my-3">
            <div class="card-body">
                <h4 class="card-title mb-3 text-center">
                    {{$postComment->content}}
                </h4>
                <h6 class="card-title mb-3 text-end">
                    Posted By: {{$postComment->user->name}}
                </h6>
                <h6 class="card-title mb-3 text-end">
                    Created At: {{$postComment->created_at}}
                </h6>
            </div>
        </div>
    @endforeach

@endsection