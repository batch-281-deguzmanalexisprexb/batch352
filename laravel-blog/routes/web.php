<?php

use Illuminate\Support\Facades\Route;
// link the Post Controller Class
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// define a route wherein to create a post will be returned to the user

Route::get('/posts/create', [PostController::class, 'create'] );

// define a route wherein form data will be sent via POST method to the /post URI endpoint.
Route::post('/posts', [PostController::class, 'store']);

// define a route that will return a view containing all posts
Route::get('/posts', [PostController::class, 'index']);

// so2 activity
// define a route that will return a view for the welcome page
Route::get('/', [PostController::class, 'welcome']);

// define a route wherein a view containing only the authenticated user's post.
Route::get('/myPosts', [PostController::class, 'myPosts']);

// define a route wherein a view showing a specific post with matching URL parameter 'id' will be returned to the user.
Route::get('/posts/{id}', [PostController::class, 'show']);

Route::get('/posts/{id}/edit', [PostController::class, 'edit']);

// define a route that will overwrite an existing post with matching url parameter via PUT method
Route::put('/posts/{id}', [PostController::class, 'update']);

// define a route that will delete a post of matching URL parameter ID
// Route::delete('/posts/{id}', [PostController::class, 'destroy']);

// Activity s04
Route::delete('/posts/{id}', [PostController::class, 'archive']);

// define a route that will call the like action when a PUT request is received at the /post/{id}/like endpoint
Route::put('/posts/{id}/like', [PostController::class, 'like']);

Route::put('/posts/{id}/comment', [PostController::class, 'comment']);